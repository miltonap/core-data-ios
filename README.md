# Core Dada iOS
---
## Swift Objects Use
- NSFetchedResultsController
- NSManagedObjectContext
- NSManagedObjectModel
- NSPersistenStoreCoordinator
- TableView
- TableViewCell
---
## Capabilities
- delete
- insert
- update
- fetch
- use NSF
