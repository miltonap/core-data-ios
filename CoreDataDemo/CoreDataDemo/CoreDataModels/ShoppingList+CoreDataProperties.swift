//
//  ShoppingList+CoreDataProperties.swift
//  CoreDataDemo
//
//  Created by Milton Palaguachi on 1/23/21.
//
//

import Foundation
import CoreData


extension ShoppingList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ShoppingList> {
        return NSFetchRequest<ShoppingList>(entityName: "ShoppingList")
    }

    @NSManaged public var name: String?

}
