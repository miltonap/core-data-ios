//
//  CoreDataManager.swift
//  CoreDataDemo
//
//  Created by Milton Palaguachi on 1/21/21.
//
import CoreData
import Foundation
//MARK: -  CoreData Manager
class CoreDataManager<T: NSManagedObject> {
    //MARK: - Properties
    var modelName: String
    
    //MARK: - Initializer
    init(modelName: String  = "ShoppingListModel") {
        self.modelName = modelName
    }
    
    //MARK: - Persistent Store Coordinator
    
    lazy var persitentStoreCoordinator: NSPersistentStoreCoordinator = {
        let persistenStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        do {
            try  persistenStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: self.destinatioStoreURL, options: nil)

        } catch {
            fatalError("Unable to load persisten store, error \(error)")
        }
        return persistenStoreCoordinator
    }()
  
    // MARK: - Managed Object Model
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        guard let modelURL = Bundle.main.url(forResource: self.modelName, withExtension: "momd") else { fatalError("UserModel not fount in Bundle") }
        guard let manegedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else { fatalError("Unable to initialize ManagedObjectModel") }
        return manegedObjectModel
    }()
    
    // MARK: - Managed Object Context
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persitentStoreCoordinator
        return managedObjectContext
    }()
    
    // MARK: - Destination Dada Base path
    
    private var destinatioStoreURL: URL {
        guard let documenetURl = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first else { fatalError("Unable to find document directory") }
        print(documenetURl.absoluteURL)
        return documenetURl.appendingPathComponent("\(self.modelName).sqlite")
    }
    
    // MARK: - Save Changes to Managed Object Context
    public func managedObjectContextSave() {
        do {
            try self.managedObjectContext.save()
        } catch  {
            print("error - \(error)")
        }
    }
}

