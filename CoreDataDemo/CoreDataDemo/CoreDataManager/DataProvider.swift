//
//  DataProvider.swift
//  CoreDataDemo
//
//  Created by Milton Palaguachi on 1/21/21.
//

import UIKit
import CoreData

enum TableViewRowAction {
    case insert, delete,move, update
}

protocol DataProviderDelegate: AnyObject {
    func updateTableView(_ indexPath: IndexPath, type: TableViewRowAction)
}

class DataProvider<T: NSFetchRequestResult>: NSObject, NSFetchedResultsControllerDelegate {
    public var fetchResultsController: NSFetchedResultsController<T>!
    weak var delegate: DataProviderDelegate?

    public func populateShoppingLists(managedObjectContext: NSManagedObjectContext, entityName: String, sort by: String, ascending: Bool) {
        let request = NSFetchRequest<T>(entityName: entityName)
        request.sortDescriptors = [NSSortDescriptor(key: by, ascending: ascending)]
        self.fetchResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchResultsController.delegate = self
        do {
            try self.fetchResultsController.performFetch()
        } catch  {
            print("error - \(error)")
        }
    }
    
    public func objectAtIndexPath(at: IndexPath) -> T {
        return self.fetchResultsController.object(at: at)
    }

    public func sections() -> [NSFetchedResultsSectionInfo]? {
        return self.fetchResultsController.sections
    }
 
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            self.delegate?.updateTableView(newIndexPath, type: .insert)
            
        case .delete:
            guard let indexPath = indexPath else { return }
            self.delegate?.updateTableView(indexPath, type: .delete)
            
        case .move:
            print("move")
        case .update:
            print("update")
        @unknown default:
            break
        }
    }
}
