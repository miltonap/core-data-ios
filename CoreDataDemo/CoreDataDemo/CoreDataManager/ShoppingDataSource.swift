//
//  ShoppingDataSource.swift
//  CoreDataDemo
//
//  Created by Milton Palaguachi on 1/22/21.
//

import UIKit
import CoreData

class ShoppingDataSource: NSObject {
    typealias Model = ShoppingList
    
    var dataProvider: DataProvider<Model>
    var coreDataManager: CoreDataManager<Model>

    var tableView: UITableView
    
    init(_ tableView: UITableView, _ dataProvider: DataProvider<Model> = DataProvider(), _ coreDataManager: CoreDataManager<Model> = CoreDataManager()) {
        self.tableView = tableView
        self.dataProvider = dataProvider
        self.coreDataManager = coreDataManager
        super.init()
        self.setUp()
    }
    
    func setUp() {
        self.tableView.register(ShoppingTableViewCell.nib(), forCellReuseIdentifier: ShoppingTableViewCell.identifier)
        self.tableView.dataSource = self
        self.dataProvider.delegate = self
        self.dataProvider.populateShoppingLists(managedObjectContext: self.coreDataManager.managedObjectContext, entityName: "ShoppingList", sort: "name", ascending: true)
    }

    public func insert(object: Any){
        guard let text = object as? String else {return }
        let shoppingList = ShoppingList(context: self.coreDataManager.managedObjectContext)
        shoppingList.name = text
        self.coreDataManager.managedObjectContextSave()
    }
  
    public func delete(_ indexPath: IndexPath) {
        let object = self.dataProvider.objectAtIndexPath(at: indexPath)
        self.coreDataManager.managedObjectContext.delete(object)
        self.coreDataManager.managedObjectContextSave()
    }
    
}

extension ShoppingDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.dataProvider.sections() else { return 0 }
        return sections[section].numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let object = self.dataProvider.objectAtIndexPath(at: indexPath)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingTableViewCell", for: indexPath) as? ShoppingTableViewCell else {
            fatalError("Unable to dequeue tableViewCell with identifier : ShoppingTableViewCell")
        }
        cell.configure(name: object.name ?? "")
        return cell
    }
    
}

extension ShoppingDataSource: DataProviderDelegate {
    
    func updateTableView(_ indexPath: IndexPath, type: TableViewRowAction) {
        switch type {
        case .insert:
            self.tableView.insertRows(at: [indexPath], with: .automatic)
            
        case .delete:
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        default: break
        }
    }
    
}
