//
//  ShoppingTableViewCell.swift
//  CoreDataDemo
//
//  Created by Milton Palaguachi on 1/21/21.
//

import UIKit

class ShoppingTableViewCell: UITableViewCell {
    
    static let identifier = "ShoppingTableViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "ShoppingTableViewCell", bundle: nil)
    }
    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configure(name: String) {
        self.nameLabel.text = name
    }
}
