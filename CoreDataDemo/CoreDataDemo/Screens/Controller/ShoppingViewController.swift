//
//  ViewController.swift
//  CoreDataDemo
//
//  Created by Milton Palaguachi on 1/21/21.
//
import CoreData
import UIKit

class ShoppingViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var shoppinDataSource: ShoppingDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "ShoppingList"
        self.shoppinDataSource = ShoppingDataSource(self.tableView)
    }
}

extension ShoppingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deletAction = UIContextualAction(style: .destructive, title: "Delete") { [unowned self] action, view, completion in
            self.shoppinDataSource.delete(indexPath)
        }
        
        let swipeActionConfiguration = UISwipeActionsConfiguration(actions: [deletAction])
        swipeActionConfiguration.performsFirstActionWithFullSwipe = false
        return swipeActionConfiguration
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 44))
        headerView.backgroundColor = UIColor.lightText
        let textField = UITextField(frame: headerView.frame)
        textField.placeholder = "Add item ..."
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
        textField.leftViewMode = .always
        textField.delegate = self
        headerView.addSubview(textField)
        return headerView
    }
}

extension ShoppingViewController: UITextFieldDelegate {
    
    // called when 'return' key pressed. return NO to ignore.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextFieldShoudReturn")
        guard let text = textField.text, !text.isEmpty else { return true }
        self.shoppinDataSource.insert(object: text)
        textField.text = nil
        textField.placeholder = "Add item ..."
        textField.resignFirstResponder()
        return true
    }
}
